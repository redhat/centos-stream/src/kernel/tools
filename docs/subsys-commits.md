subsys-commits.py
=================

Get a list of commits to backport for the given subsystem.

Features:
* no configuration needed
* gets file paths from `owners.yaml` automatically
* identifies fixes
* identifies upstream patch sets
* excludes already backported patches automatically
* considers partially backported patches
* output can be fed to `git-backport`

Usage in Examples
-----------------

Get commits to backport to the latest RHEL for the `i40e` driver:


```
./subsys-commits.py i40e
```

Use RHEL 8.8 instead:

```
./subsys-commits.py --rhel 8.8 i40e
```

Only include commits up to upstream v5.16; note that later fixes *will* be
still included:

```
./subsys-commits.py --rhel 8.8 --upstream 5.16 i40e
```

Output Format in Examples
-------------------------

This is part of output for `subsys-commits.py mlx5`:

```
d671e109bd85 *  net/mlx5: Fix tc max supported prio for nic mode
ec2fa47d7b98 *  net/mlx5: Lag, use lag lock
144d4c9e800d %  flow_offload: reject to offload tc actions in offload drivers
4cd14d44b11d *  net/mlx5: Support devices with more than 2 ports
4b83c3caf289    └─ RDMA/mlx5: Use the proper number of ports
367dfa121205 *┐ net/mlx5: Remove devl_unlock from mlx5_eswtich_mode_callback_enter
03f9c47d0f79 *│ net/mlx5: Use devl_ API for rate nodes destroy
868232f5cd38  │ devlink: Remove unused function devlink_rate_nodes_destroy
f1bc646c9a06 *│ net/mlx5: Use devl_ API in mlx5_esw_offloads_devlink_port_register
da212bd29d7f *│ net/mlx5: Use devl_ API in mlx5_esw_devlink_sf_port_register
df539fc62b06  │ devlink: Remove unused functions devlink_rate_leaf_create/destroy
7b19119f4c7d *│ net/mlx5: Use devl_ API in mlx5e_devlink_port_register
973598d46ede *│ net/mlx5: Remove devl_unlock from mlx5_devlink_eswitch_mode_set
f0680ef0f949 *┘ devlink: Hold the instance lock in port_new / port_del callbacks
ca2bb69514a8  ┐ geneve: do not use RT_TOS for IPv6 flowlabel
b690842d12fd ?⋮ ├─ selftests/net: test l2 tunnel TOS/TTL inheriting
88e500affe72 ?⋮ └─ selftests/net: fix reinitialization of TEST_PROGS in net self tests.
e488d4f5d6e4  │ vxlan: do not use RT_TOS for IPv6 flowlabel
bcb0da7fffee *│ mlx5: do not use RT_TOS for IPv6 flowlabel
ab7e2e0dfa5d  ┘ ipv6: do not use RT_TOS for IPv6 flowlabel
```

Let's go through it line by line.

```
d671e109bd85 *  net/mlx5: Fix tc max supported prio for nic mode
ec2fa47d7b98 *  net/mlx5: Lag, use lag lock
```

The commits marked by `*` directly modified a path listed in `onwers.yaml`.

```
144d4c9e800d %  flow_offload: reject to offload tc actions in offload drivers
```

The `%` mark means that the commit was already backported but only
partially. Part of it may still be needed. Note that the tool does not look
into the commit to determine whether the relevant part was backported or
not. That's up to you; you can use the `--exclude` option to ignore the
commit in the future runs (see the next section).

```
4cd14d44b11d *  net/mlx5: Support devices with more than 2 ports
4b83c3caf289    └─ RDMA/mlx5: Use the proper number of ports
```

The commit got a follow up fix upstream. You should most likely include the
fix, too.

```
367dfa121205 *┐ net/mlx5: Remove devl_unlock from mlx5_eswtich_mode_callback_enter
03f9c47d0f79 *│ net/mlx5: Use devl_ API for rate nodes destroy
868232f5cd38  │ devlink: Remove unused function devlink_rate_nodes_destroy
f1bc646c9a06 *│ net/mlx5: Use devl_ API in mlx5_esw_offloads_devlink_port_register
da212bd29d7f *│ net/mlx5: Use devl_ API in mlx5_esw_devlink_sf_port_register
df539fc62b06  │ devlink: Remove unused functions devlink_rate_leaf_create/destroy
7b19119f4c7d *│ net/mlx5: Use devl_ API in mlx5e_devlink_port_register
973598d46ede *│ net/mlx5: Remove devl_unlock from mlx5_devlink_eswitch_mode_set
f0680ef0f949 *┘ devlink: Hold the instance lock in port_new / port_del callbacks
```

The commits of interest (marked by `*`) were part of a patch set upstream.
Often, it is best to take the whole set. Use your own judgement.

```
ca2bb69514a8  ┐ geneve: do not use RT_TOS for IPv6 flowlabel
b690842d12fd ?⋮ ├─ selftests/net: test l2 tunnel TOS/TTL inheriting
88e500affe72 ?⋮ └─ selftests/net: fix reinitialization of TEST_PROGS in net self tests.
e488d4f5d6e4  │ vxlan: do not use RT_TOS for IPv6 flowlabel
bcb0da7fffee *│ mlx5: do not use RT_TOS for IPv6 flowlabel
ab7e2e0dfa5d  ┘ ipv6: do not use RT_TOS for IPv6 flowlabel
```

This is another example of an upstream patch set. This time, a patch set
that we likely won't be taking in full in our backport.

The `?` flag marks uncertain follow ups. Those are commits that mention the
commit "geneve: do not use RT_TOS for IPv6 flowlabel" somewhere in their
description. They are often not needed but sometimes they point out an
important fix or follow up.

Excluding Commits
-----------------

If you don't want to backport a certain commit and want it stop showing
again and again in the output, you can add it to an exclude list:

```
./subsys-commits.py --exclude 144d4c9e800d
```

If you want to exclude it only for RHEL 9:


```
./subsys-commits.py --exclude --rhel 9 144d4c9e800d
```

Or exclude it for RHEL 8.x starting from 8.7; note this does *not* exclude
it for RHEL 9:

```
./subsys-commits.py --exclude --rhel 8.7 144d4c9e800d
```
