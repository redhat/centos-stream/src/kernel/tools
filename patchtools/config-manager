#!/bin/bash
#
# config-manager

declare MYDIR
MYDIR="$(dirname "$(which "$(basename "$0")")")"
[ "$MYLIB" ] || declare MYLIB=$MYDIR/lib
source "$MYLIB"/ui.source

declare b_readop=false
declare b_writeop=false
declare configfile=
declare key=
declare value=
declare usagestr=

usagestr=$(
cat <<EOF

$(basename "$0") -f config-file -<r|w> key [new value] [-dark | -light]]

Description:
	Simple configuration file manager. The configuration file simply
	consists of a key : value, where value can be any scalar type.

Arguments:
	-h : this help text
	-f : path to config file
	-k : the key in the config file to access
	-r : read the value of the key stored in the config file
	-w <value> : write the key with a new value
	-dark | -light : this option sets the message color scheme
	                 for dark or light terminal operation.
			 You can alias config-manager for either,
			 e.g...
			 alias config-manager='config-manager -dark'

EXample:
	$(basename "$0") -f myconfig -w somekey newval -b 2

\0
EOF
)

declare -i aok=0
declare -i err_nokey=$((ui_err_boundary + 1))
declare -i err_invopt=$((ui_err_boundary + 2))
declare -i err_invoper=$((ui_err_boundary + 3))

ui_err_msg+=(
	"Key not found"
	"Invalid option"
	"Invalid operation"
)

# run if user hits control-c
#
control_c()
{
	echo -en "\nCtrl-c detected\nCleaning up and exiting.\n"
	exit $CTLC_EXIT
}

usage() {
	echo -en "$usagestr"
}

exit_err() {
	local err=$1
	local val="$2"

	echo -e "\n$WRN${ui_err_msg[$err]} $INF: $STA$val$OFF"
	usage
	exit "$err"
}

set_color() {
	b_color=true
	terminal_background=$1
	ui_set_colors
}

set_write() {
	key="$1"
	value="$2"
	# local twoargs="-w requires two args, key and value."

	# [ $# -eq 2 ] || err_exit $ui_err_invargc "$twoargs"
	# [ ${value:0:1} == "-" ] && err_exit $err_invopt "$twoargs"
	[ "${value:0:1}" == "-" ] && value=
	$b_readop && err_exit $err_invoper "Cannot write when read was requested."
}

set_read() {
	key="$1"

	[ $# -eq 1 ] || err_exit $ui_err_invargc "-r requires key name"
	$b_writeop && err_exit $err_invoper "Cannot read when write was requested."
}

parse_cmd() {
	local opt
	local b_err=false
	local erropt

	while [ -n "$1" ]; do
		opt="$1"
		case "$opt" in
			"-f" ) configfile="$2"; shift 2;;
			"-r" ) b_readop=true; key=$2; shift 2;;
			"-w" ) b_writeop=true; key=$2; value="$3"; shift 3;;
			"-h" ) usage; exit $aok;;
			"-b" ) set_color "$2"; shift 2;;
			"-dark" ) set_color $tb_dark; shift 1;;
			"-light" ) set_color $tb_lite; shift 1;;
			* ) b_err=true; erropt="$1"; shift;;
		esac
	done

	$b_err && exit_err $err_invopt "$erropt"
	[ -f "$configfile" ] || exit_err $ui_err_invfile "$configfile"
	grep -w -q "$key" "$configfile" || exit_err $err_nokey "$key"
}

read_key() {
	local keyline
	local ary
	keyline=$(grep -w "$key" "$configfile")
	ui_strtok "$keyline" "= " ary
	echo "${ary[1]}"
}

write_key() {

	local line
	local -i linenumber

	line=$(grep -n "$key" "$configfile")
	linenumber=$(echo "$line" | cut -d":" -f1)

	line=$(echo "$line" | cut -d":" -f2- | cut -d"=" -f1)
	line="$line""= $value"
	ui_replaceline_raw "$linenumber" "$line" "$configfile"
}

main() {
	parse_cmd "$@"
	($b_readop && $b_writeop) && exit_err $err_invoper "only one of -r or -w allowed."
	$b_readop && read_key
	$b_writeop && write_key
}

# Trap for control-c
#
trap control_c SIGINT

main "$@"

exit 0

