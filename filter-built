#!/usr/bin/env python3
# SPDX-License-Identifier: GPL-2.0

# Given a set of config files, the script filter commits from the
# find-backports script that are actually built in the kernel.
# The algorithm is far from precise. The main usage for this
# is for mining silent security commits in upstream.

import argparse
import os
import re
import sys
import subprocess
from dataclasses import dataclass
from pathlib import Path
from typing import Iterable
from collections.abc import Set

join_lines_re = re.compile(r"\\s*\n\s*")
split_assigment_re = re.compile(r"")
config_re = re.compile(r"(\w+)-([ym]|\$\(([A-Z0-9_]+)\))?")
var_re = re.compile(r"\$\(([A-Z0-9_]+)\)")

valid_archs = frozenset(("x86", "x86_64", "s390", "arm", "arm64", "powerpc"))


def is_not_comment(line: str) -> bool:
    """Return a bool indicating the line is not a comment line."""
    return len(line) != 0 and not line.startswith("#")


def get_configs(config_file: Path) -> set[str]:
    """Parse the kernel config file.

    It returns a set() with all configs defined.
    """
    return set(
        map(
            lambda line: line.split("=")[0].strip(),
            filter(is_not_comment, config_file.read_text().splitlines()),
        )
    )


def parse_makefile(contents: Iterable[str]) -> Set[str]:
    """Parse the makefile.

    Return the set of object files that are built by the Makefile.
    """
    fset: set[str] = set()

    def add(files: Iterable[str]) -> None:
        for f in files:
            fset.add(f.rstrip(",/"))

    for fields in map(lambda l: l.split(), contents):
        if len(fields) < 3:
            continue

        m = config_re.match(fields[0])
        if not m:
            # print(f"Error parsing '{fields[0]}'", file=sys.stderr)
            continue

        files = fields[2:]

        match m.groups():
            case [
                "ccflags"
                | "condflags"
                | "asflags"
                | "ldflags"
                | "subdir-ccflags"
                | "subdir-asflags"
                | "ccflags-remove"
                | "asflags-remove",
                _,
                _,
            ]:
                pass

            case [
                "core"
                | "drivers"
                | "libs"
                | "obj"
                | "ipv6-objs"
                | "lib"
                | "always"
                | "extra",
                "y" | "m",
                None,
            ]:
                add(files)

            case [target, "y" | "m", None] if f"{target}.o" in fset:
                add(files)

    return fset


def read_makefile(makefile: Path, configs: Set[str]) -> Iterable[str]:
    """Read the makefile, replacing config vars."""

    def repl(m: re.Match) -> str:
        return "y" if m.group(1) in configs else ""

    text = re.sub(var_re, repl, makefile.read_text())

    return filter(
        is_not_comment,
        re.sub(join_lines_re, " ", text).splitlines(),
    )


@dataclass
class MakefileProcessor:
    configs: Set[str]

    def __call__(self, makefile: Path) -> Set[str]:
        return parse_makefile(read_makefile(makefile, self.configs))


def get_commit_files(commit: str) -> Iterable[str]:
    cmd = f"git log -1 --name-only --format='' {commit}"
    p = subprocess.run(cmd, text=True, check=True, shell=True, stdout=subprocess.PIPE)
    for f in p.stdout.splitlines():
        yield f


def is_built(filename: os.PathLike | str, mkp: MakefileProcessor) -> bool:
    """Check if a source file is built.

    For anything other than a .c source file, it returns true.
    """
    p = Path(filename).resolve().relative_to(Path.cwd())

    parts = p.parts

    if not p.exists():
        return False

    try:
        i = parts.index("arch")
    except ValueError:
        # For file not under arch/, walkthrough
        # the directory hierarchy  and check if
        # the subdirs are part of the build
        partial = p.parent
        while partial.name:
            name = partial.name

            # for include files, we always consider
            # they are part of the build.
            if name == "include":
                break

            partial = partial.parent

            makefile = partial / "Makefile"
            if not makefile.exists():
                continue

            dirset = mkp(makefile)
            if dirset is None or name not in dirset:
                return False
    else:
        if i < len(parts) - 1 and parts[i + 1] not in valid_archs:
            return False

    if p.suffix == ".h":
        return True

    if p.suffix != ".c":
        return False

    makefile = p.parent / "Makefile"
    if not makefile.exists():
        makefile = p.parent.parent / "Makefile"
        if not makefile.exists():
            return True

    fileset = mkp(makefile)
    return p.with_suffix(".o").name in fileset


def main() -> None:
    parser = argparse.ArgumentParser(
        prog="filter-built",
        description="Verifies if the given source files are included a kernel build",
    )
    parser.add_argument(
        "-c, --config",
        action="store",
        dest="configs",
        nargs="*",
        default=[".config"],
        help="Kernel config file. It accepts glob syntax to specify multiple files.",
    )
    args = parser.parse_args()

    configs: Set[str] = set().union(*(get_configs(Path(cfg)) for cfg in args.configs))

    mkp = MakefileProcessor(configs)

    for line in sys.stdin:
        if not line.strip():
            continue

        commit = line.split()[0]

        for f in get_commit_files(commit):
            if is_built(f, mkp):
                print(line.lstrip(), end="")
                break


if __name__ == "__main__":
    main()
